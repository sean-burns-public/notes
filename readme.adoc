= Notes
:author: Sean Burns
:toc: preamble
:toclevels: 1
:toc-title: Contents

The home of all my public notes which are accessible via GitLab.

== Public Notes

:leveloffset: +2

:subfolder: black_magic_probe
include::{subfolder}/readme.adoc[]

:subfolder: fast_usb_asp
include::{subfolder}/readme.adoc[]
